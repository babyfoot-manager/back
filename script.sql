CREATE TABLE partie(
   id  SERIAL PRIMARY KEY,
   name         VARCHAR(255) not null default '',
   finish		BOOLEAN not null default false
);

CREATE TABLE babyfoot_user(
   id  SERIAL PRIMARY KEY,
   name         VARCHAR(255) not null default ''
);

CREATE TABLE chat(
   id  SERIAL PRIMARY KEY,
   message         text not null default '',
   message_date   		   timestamp,
   user_id		   INTEGER REFERENCES babyfoot_user(id)
);