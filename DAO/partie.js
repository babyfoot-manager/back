const db = require('./queries')

const getAllParties = (request, response) => {
    db.pool.query('SELECT id,name,finish FROM partie ORDER BY id ASC', (error, results) => {
        if (error) {
            throw error;
        }
        response.status(200).json(results.rows);
    });
}

const addPartie = (request, response) => {
    const name = request.body.name

    db.pool.query('INSERT INTO partie (name,finish) VALUES ($1, $2)', [name, false], (error, results) => {
        if (error) {
            throw error;
        }
        response.status(201).send();
    });
};

const updatePartie = (request, response) => {
    const id = parseInt(request.params.id)
    const finish = request.body.finish
    db.pool.query('UPDATE partie SET finish =$1 WHERE id = $2', [finish, id], (error, results) => {
        if (error) {
            throw error;
        }
        response.status(200).send();
    });
};

const deletePartie = (request, response) => {
    const id = parseInt(request.params.id)
    db.pool.query('DELETE FROM partie WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).send()
    });
};

module.exports = {
    getAllParties,
    addPartie,
    updatePartie,
    deletePartie,
}