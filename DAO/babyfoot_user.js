const db = require('./queries')

const addUser = async (request, response) => {
    const name = request.body.name;
    let user = await getUser(name);
    if (user === undefined) {
        db.pool.query('INSERT INTO babyfoot_user (name) VALUES ($1)', [name], (error, results) => {
            if (error) {
                throw error
            }
        });
        user = await getUser(name);
    }
    if (user != undefined) {
        response.status(201).send(user);
    }
    else {
        response.status(201).send();
    }    
};


async function getUser(name) {
    const user = await db.pool.query('SELECT id,name FROM babyfoot_user WHERE name = $1 ORDER BY id ASC', [name]);
    return user.rows[0]
}

module.exports = {
    addUser,
}