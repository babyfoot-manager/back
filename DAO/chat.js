const db = require('./queries')

const getAllChats = (request, response) => {
    db.pool.query(`SELECT chat.id,message,message_date,user_id,name AS user_name 
    FROM chat 
    INNER JOIN babyfoot_user ON chat.user_id = babyfoot_user.id
    ORDER BY message_date ASC`, (error, results) => {
        if (error) {
            throw error;
        }
        response.status(200).json(results.rows);
    });
}

const addChat = (request, response) => {
    const message = request.body.message
    const user_id = request.body.userId
    db.pool.query('INSERT INTO chat (message,message_date,user_id) VALUES ($1, to_timestamp($2 / 1000.0), $3)', [message, Date.now(), user_id], (error, results) => {
        if (error) {
            throw error;
        }
        response.status(201).send();
    });
};

module.exports = {
    getAllChats,
    addChat,
}