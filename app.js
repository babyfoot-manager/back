require('dotenv').config()
const partie = require('./DAO/partie');
const user_babyfoot = require('./DAO/babyfoot_user');
const chat = require('./DAO/chat');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 8080;
const server = require('http').createServer();
const io = require('socket.io')(server);

//API

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://127.0.0.1");
    res.header('Access-Control-Allow-Headers', 'content-type');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use(bodyParser.json())

app.get('/', (request, response) => {
    response.json({ info: 'back-end babyfoot manager' })
})

//partie
app.get('/parties', partie.getAllParties);
app.post('/partie', partie.addPartie);
app.put('/partie/:id', partie.updatePartie);
app.delete('/partie/:id', partie.deletePartie);

//user
app.post('/user', user_babyfoot.addUser);

//chat
app.get('/chats', chat.getAllChats);
app.post('/chat', chat.addChat);

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})

//Websocket
io.on('connection', socket => {

    socket.on('refreshParties', data => {
        socket.broadcast.emit('refreshParties');
    });

    socket.on('refreshChats', data => {
        socket.broadcast.emit('refreshChats');
    });

    socket.on('typingStart', data => {
        socket.broadcast.emit('typingStart', data);
    });

    socket.on('typingEnd', data => {
        socket.broadcast.emit('typingEnd');
    });

});

server.listen(3000);