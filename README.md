# Back bayfoot Manager

Back-end de l'application bayfoot Manager.
BabyFoot Manager est une application web de type RIA permettant de créer des parties de babyfoot. Sa particularité sera de pouvoir créer des parties de manière collaborative.

## Prérequis

* PostgreSQL 12
* nodeJS 12


### Pour commencer 

Récupérer les sources

```
git clone https://gitlab.com/babyfoot-manager/back
cd back
npm install
```

### Base de données

Il faut créer une base de données et y exécuter le script sql : "script.sql"

### Environnement  

Ensuite à la racine de ce projet créer un fichier ".env" contenant les informations de connexions à la base de données :

```
DB= databaseName
DB_HOST= databaseHost
DB_USER= databaseUser
DB_PASS= databasePassword
DB_PORT= databasePort
```

### Démarrer le serveur

```
npm run start
```
Ouvrir [http://localhost:8080](http://localhost:8080) pour vérifier le lancement du serveur.
